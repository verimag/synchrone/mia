#=========================================================================================
		Description of the examples
#=========================================================================================


In this folder, different examples are implemented.

The following examples : 
  - ex1
  - ex2
  - ex3
  - ex4
are some test experiments using the new model of the NoC RX access. 
For each .yaml file there is a corresponding .png file describing the task graph.

  - rosace : Implementation of the Rosace example using the new model
  of NoC RX access.

The "profiles" folder gather the different task profiles corresponding to each example.


#========================================================================================
		How to test the different examples using MIA ?
#========================================================================================

To test these examples, you'll have to change the environment variables, so that it 
matches the new configuration:

$> export ROOT=${path_to_project}/mia-master/
$> export ARCHS=$ROOT/archs
$> export PROFILES=$ROOT/examples/NoC/profiles

If you use MIA Viewer, you'll have to switch the Workspace to : 
${path_to_project}/examples/NoC/


