#! /usr/bin/env python3

# MIA (Many-Core Interferece Analysis) - Tool that computes minimal release dates
# of a given scheduled task set on a many-core processor, taking interference into
# account
# Copyright (C) 2019-2020 Verimag
#
# This file is part of MIA (Many-Core Interferece Analysis).
#
# MIA (Many-Core Interferece Analysis) is free software: you can redistribute it
# and/or modify
# it under the terms of the CeCILL-C License as published by the CEA - CNRS -
# INRIA association, either version 2.1 of the License, or (at your option) any
# later version.
#
# MIA (Many-Core Interferece Analysis) is distributed "as is" in the hope that it
# will be
# useful, but WITHOUT ANY OTHER EXPRESS OR TACIT WARRANTY, and in particular,
# without any warranty as to its COMMERCIAL value, its secured, safe, innovative
# or relevant nature.  See the CeCILL-C License for more details.
#
# You should have received a copy of the CeCILL-C License along with
# MIA (Many-Core Interferece Analysis).  If not, see
# <https://cecill.info/licences.en.html>.


import yaml
import argparse
import re


def textbf(s):
    return r'\textbf{' + str(s) + r'}'


def main():
    parser = argparse.ArgumentParser(description='Read a Yaml task graph annotated \
    with release dates and response times, and produce a LaTeX array.')
    parser.add_argument("input", type=str, nargs='+',
                        help='Yaml input file(s)')
    args = parser.parse_args()
    with open(args.input[0], 'r') as stream:
        try:
            data = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    args.compare = (len(args.input) == 2)

    if args.compare:
        with open(args.input[1], 'r') as stream:
            try:
                data2 = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)

    for part in 'nodes', 'RxAccesses':
        for name, content in data[part].items():
            m = re.match('([a-zA-Z]*)([0-9]*)', name)
            if m:
                latex_name = r'$\text{' + m.group(1) + r'}_{' + m.group(2) + r'}$'
            else:
                latex_name = name
            latex_name = latex_name.ljust(20)
            print(f"{latex_name}", end='')
            for field in 'release', 'response':
                value1 = content[field]
                if args.compare:
                    node2 = data2[part][name]
                    value2 = node2[field]
                    if value1 != value2:
                        value1 = textbf(value1)
                        value2 = textbf(value2)
                    value1 = str(value1).ljust(15)
                    value2 = str(value2).ljust(15)
                    print(f"& {value1} & {value2}", end='')
                else:
                    print(f"\t&\t{value1}", end='')
            print("\t\\\\ \\hline")


if __name__ == "__main__":
    main()
