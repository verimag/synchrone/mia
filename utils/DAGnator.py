#!/usr/bin/env python3

# MIA (Many-Core Interferece Analysis) - Tool that computes minimal release dates
# of a given scheduled task set on a many-core processor, taking interference into
# account
# Copyright (C) 2019-2020 Verimag
#
# This file is part of MIA (Many-Core Interferece Analysis).
#
# MIA (Many-Core Interferece Analysis) is free software: you can redistribute it
# and/or modify
# it under the terms of the CeCILL-C License as published by the CEA - CNRS -
# INRIA association, either version 2.1 of the License, or (at your option) any
# later version.
#
# MIA (Many-Core Interferece Analysis) is distributed "as is" in the hope that it
# will be
# useful, but WITHOUT ANY OTHER EXPRESS OR TACIT WARRANTY, and in particular,
# without any warranty as to its COMMERCIAL value, its secured, safe, innovative
# or relevant nature.  See the CeCILL-C License for more details.
#
# You should have received a copy of the CeCILL-C License along with
# MIA (Many-Core Interferece Analysis).  If not, see
# <https://cecill.info/licences.en.html>.


"""
    This script is used to generate random graphs.
"""

import os
import argparse

import yaml
from random import randint, random


def add_node(number):
    data['nodes'][f"n{number}"] = {}
    dnode = data['nodes'][f"n{number}"]
    dnode['assignedTo'] = number % NUM_OF_CORES
    dnode['order'] = number // NUM_OF_CORES
    dnode['release'] = 0
    dnode['deps'] = []
    dnode['accessTo'] = []
    dnode['traceName'] = f"{basename[:-5]}_n{number}-trace"
    datfile = os.path.join(profiles,
                           f"baseArchitecture/{basename[:-5]}_n{number}-trace.dat")
    with open(datfile, 'w') as outfile:
        wcet = randint(550, 650)
        MD = randint(250, 550)
        WCBT = MD
        outfile.write(f"procDemand {wcet}\n")
        outfile.write(f"memDemand {MD}\n")
        outfile.write(f"nbTransactions {WCBT}\n")


def add_nodes():
    for i in range(NUM_LAYERS * VERTICES_PER_LAYER):
        add_node(i)


def add_edges():
    for L in range(NUM_LAYERS - 1):
        for i in range(VERTICES_PER_LAYER):
            for j in range(VERTICES_PER_LAYER):
                if random() < BRANCH_PROBABILITY:
                    source = VERTICES_PER_LAYER * L + i
                    dest = VERTICES_PER_LAYER * (L + 1) + j
                    add_edge(source, dest)


def add_edge(source, dest):
    # The source accesses the destination.
    source_dnode = data['nodes'][f"n{source}"]
    dest_dnode = data['nodes'][f"n{dest}"]
    if f"n{dest}" not in [node for node, _ in source_dnode['accessTo']]:
        source_dnode['accessTo'] += [[f"n{dest}", randint(0, 100)]]
        dest_dnode['deps'] += [f"n{source}"]


if __name__ == "__main__":
    # Argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument("-noc", "--num-of-cores", type=int)
    parser.add_argument("-LS", "--layer-size", type=int)
    parser.add_argument("-NL", "--num-layers", type=int)
    parser.add_argument("-bp", "--branch-probability", type=float)
    parser.add_argument("-profiles", type=str)
    parser.add_argument("-o", "--output", type=str, default='random.yaml')

    args = parser.parse_args()

    NUM_OF_CORES = args.num_of_cores
    VERTICES_PER_LAYER = args.layer_size
    NUM_LAYERS = args.num_layers
    BRANCH_PROBABILITY = args.branch_probability

    profiles = args.profiles

    dirname = os.path.dirname(args.output)
    basename = os.path.basename(args.output)


    data = {}
    data['numOfCores'] = NUM_OF_CORES
    data['deadline'] = 10000000000000
    data['nodes'] = {}

    add_nodes()
    add_edges()

    with open(os.path.join(dirname, basename), 'w') as outfile:
        yaml.dump(data, outfile, default_flow_style=None)
