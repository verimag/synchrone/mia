#!/usr/bin/env python3

# MIA (Many-Core Interferece Analysis) - Tool that computes minimal release dates
# of a given scheduled task set on a many-core processor, taking interference into
# account
# Copyright (C) 2019-2020 Verimag
#
# This file is part of MIA (Many-Core Interferece Analysis).
#
# MIA (Many-Core Interferece Analysis) is free software: you can redistribute it
# and/or modify
# it under the terms of the CeCILL-C License as published by the CEA - CNRS -
# INRIA association, either version 2.1 of the License, or (at your option) any
# later version.
#
# MIA (Many-Core Interferece Analysis) is distributed "as is" in the hope that it
# will be
# useful, but WITHOUT ANY OTHER EXPRESS OR TACIT WARRANTY, and in particular,
# without any warranty as to its COMMERCIAL value, its secured, safe, innovative
# or relevant nature.  See the CeCILL-C License for more details.
#
# You should have received a copy of the CeCILL-C License along with
# MIA (Many-Core Interferece Analysis).  If not, see
# <https://cecill.info/licences.en.html>.


"""
This script is used to revert the import process done by forward.py. It mainly
restores the original DAT content that has been merged into a YAML file. A new
file named _{base_file}_{node}-trace.dat is created for each node.
"""

import yaml
import sys
import os


if __name__== "__main__":
    file = sys.argv[1]
    dirname = os.path.dirname(file)
    basename = os.path.basename(file)

    with open(file, 'r') as stream:
        try:
            data = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    for node, dnode in data['nodes'].items():
        dnode['traceName'] = f"{basename[:-5]}_{node}-trace"
        datfile = os.path.join(dirname, f"profiles/baseArchitecture/{basename[:-5]}_{node}-trace.dat")
        with open(datfile, 'w') as outfile:
            outfile.write(f"procDemand {dnode.get('procDemand', 0)}\n")
            outfile.write(f"memDemand {dnode.get('memDemand', 0)}\n")
            outfile.write(f"nbTransactions {dnode.get('nbTransactions', 0)}\n")

    with open(os.path.join(dirname, '_' + basename), 'w') as outfile:
        yaml.dump(data, outfile, default_flow_style=None)

