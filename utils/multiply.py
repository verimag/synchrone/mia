#!/usr/bin/env python3

# MIA (Many-Core Interferece Analysis) - Tool that computes minimal release dates
# of a given scheduled task set on a many-core processor, taking interference into
# account
# Copyright (C) 2019-2020 Verimag
#
# This file is part of MIA (Many-Core Interferece Analysis).
#
# MIA (Many-Core Interferece Analysis) is free software: you can redistribute it
# and/or modify
# it under the terms of the CeCILL-C License as published by the CEA - CNRS -
# INRIA association, either version 2.1 of the License, or (at your option) any
# later version.
#
# MIA (Many-Core Interferece Analysis) is distributed "as is" in the hope that it
# will be
# useful, but WITHOUT ANY OTHER EXPRESS OR TACIT WARRANTY, and in particular,
# without any warranty as to its COMMERCIAL value, its secured, safe, innovative
# or relevant nature.  See the CeCILL-C License for more details.
#
# You should have received a copy of the CeCILL-C License along with
# MIA (Many-Core Interferece Analysis).  If not, see
# <https://cecill.info/licences.en.html>.


"""
This script is used to multiply a YAML node graph description n times, for
testing purposes.

Usage:

    ./multiply.py FILE TIMES OFFSET

Where OFFSET will be the minimal release date for the second copy, OFFSET*2 for
the third, and so on.
"""

import yaml
import sys
import os
import copy


def multiply(data, count, dec):
    l = len(data['nodes'])
    i = l
    new_nodes = {}
    S = [0] * data['numOfCores']
    for dnode in data['nodes'].values():
        S[dnode['assignedTo']] = max(S[dnode['assignedTo']], dnode['order'] + 1)
    for c in range(1, count):
        for dnode in data['nodes'].values():
            node_copy = copy.deepcopy(dnode)
            if len(node_copy['deps']) > 0:
                node_copy['deps'] = [f"n{int(dep[1:]) + c*l}" for dep in node_copy['deps']]
            else:
                node_copy['deps'] = []
            if node_copy.get('accessTo', False):
                node_copy['accessTo'] = [[f"n{int(idep[1:]) + c*l}", a] for idep, a in node_copy['accessTo']]
            node_copy['release'] += c * dec
            node_copy['order'] += c * S[dnode['assignedTo']]
            new_nodes[f"n{i}"] = node_copy
            i += 1
    data['nodes'].update(new_nodes)
    return data


if __name__== "__main__":
    file = sys.argv[1]
    dirname = os.path.dirname(file)
    basename = os.path.basename(file)

    with open(file, 'r') as stream:
        try:
            data = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    count, dec = int(sys.argv[2]), int(sys.argv[3])
    data = multiply(data, count, dec)
     
    with open(os.path.join(dirname, '_' + basename), 'w') as outfile:
        yaml.dump(data, outfile, default_flow_style=None)

