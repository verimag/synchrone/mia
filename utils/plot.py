#!/usr/bin/env python3

# MIA (Many-Core Interferece Analysis) - Tool that computes minimal release dates
# of a given scheduled task set on a many-core processor, taking interference into
# account
# Copyright (C) 2019-2020 Verimag
#
# This file is part of MIA (Many-Core Interferece Analysis).
#
# MIA (Many-Core Interferece Analysis) is free software: you can redistribute it
# and/or modify
# it under the terms of the CeCILL-C License as published by the CEA - CNRS -
# INRIA association, either version 2.1 of the License, or (at your option) any
# later version.
#
# MIA (Many-Core Interferece Analysis) is distributed "as is" in the hope that it
# will be
# useful, but WITHOUT ANY OTHER EXPRESS OR TACIT WARRANTY, and in particular,
# without any warranty as to its COMMERCIAL value, its secured, safe, innovative
# or relevant nature.  See the CeCILL-C License for more details.
#
# You should have received a copy of the CeCILL-C License along with
# MIA (Many-Core Interferece Analysis).  If not, see
# <https://cecill.info/licences.en.html>.


"""
Plots the schedule using matplotlib. Usage:

    $ ./plot.py out.yaml
"""

import sys

import yaml
from matplotlib import pyplot as plt
from matplotlib.patches import Rectangle


def plot_schedule(data):
    duration = data.get('duration', 200)
    plt.figure()
    currentAxis = plt.gca()
    plt.axis('off')
    for node in data['nodes'].values():
        trace_name = node['traceName']
        core = node['assignedTo']
        rel = node['release']
        R = node['response']
        inter = node.get('interference', 0)
        if R > 0:
            currentAxis.add_patch(
                Rectangle((rel, core-0.2), R, 0.4, color='#42b6f4'))
            currentAxis.add_patch(
                Rectangle((rel + R - inter, core-0.2), inter, 0.4,
                          color='#ff5454', fill=None, hatch='////'))
            currentAxis.add_patch(
                Rectangle((rel, core-0.2), R, 0.4, fill=None))
            plt.text(rel + R / 2., core, f"r={rel}\nR={R}",
                     verticalalignment='center',
                     horizontalalignment='center', fontsize=9)
            plt.text(rel + R / 2., core + 0.3, f"{trace_name}",
                     verticalalignment='center',
                     horizontalalignment='center', fontsize=9)
    plt.axis([-1, duration + 1, -1,
              max(4, data['numOfCores']*data.get('numOfClusters', 1))])
    plt.tight_layout()
    plt.show()


if __name__ == "__main__":
    filename = sys.argv[1]
    with open(filename, 'r') as stream:
        try:
            data = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    plot_schedule(data)
