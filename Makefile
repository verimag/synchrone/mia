# Makefile used to generate a public archive from the current repo

CURR_DATE:=$(shell date +%d%m%y)
CURR_COMMIT=`git log --pretty=format:'%H' -n 1`
	
TARGET=MIA_$(CURR_DATE)
TMP_ROOT=/tmp
TMP=$(TMP_ROOT)/$(TARGET)

FILE_LIST := AUTHORS
FILE_LIST += LICENCE
FILE_LIST += README.md

MAIN=rtAnalyser.py

SRC_DIR=src
UTILS_DIR=utils

# Specify list of examples to package
EX_DIR=examples

EXAMPLES := $(EX_DIR)/example2_dflow_NoTrace.yaml
EXAMPLES += $(EX_DIR)/example2_dflow.yaml
EXAMPLES += $(EX_DIR)/multi_cluster.yaml
EXAMPLES += $(wildcard $(EX_DIR)/rtns*.yaml)
EXAMPLES += $(EX_DIR)/Coolidge5.yaml

# Specify profile traces to package
PROFILE_DIR=$(EX_DIR)/profiles/baseArchitecture

PROFILES := $(wildcard $(PROFILE_DIR)/N*-trace.dat)
PROFILES += $(wildcard $(PROFILE_DIR)/*ex3*.dat)
PROFILES += $(PROFILE_DIR)/EOT.dat
PROFILES += $(PROFILE_DIR)/_.dat

.PHONY: all clean $(TMP)

all: $(TARGET).zip

$(TARGET).zip: $(TMP)
	@cd $(TMP_ROOT) && zip -y -r $(TMP_ROOT)/tmp.zip ./$(TARGET)
	@mv $(TMP_ROOT)/tmp.zip $@
	@rm -rf $<

$(TMP):
	@mkdir -p $(TMP)
	@echo $(CURR_DATE) > $(TMP)/COMMIT
	@echo $(CURR_COMMIT) >> $(TMP)/COMMIT
	@for i in $(FILE_LIST); do \
		cp $$i $(TMP); \
	done
	@cp --preserve=links --no-dereference $(MAIN) $(TMP)
	@rm -rf $(SRC_DIR)/__pycache__
	@cp -r $(SRC_DIR) $(TMP)
	@cp -r $(UTILS_DIR) $(TMP)
	@mkdir -p $(TMP)/$(EX_DIR)
	@for i in $(EXAMPLES); do \
		cp $$i $(TMP)/$(EX_DIR)/$(EXAMPLES_DIR); \
	done
	@mkdir -p $(TMP)/$(PROFILE_DIR)
	@for i in $(PROFILES); do \
		cp $$i $(TMP)/$(PROFILE_DIR); \
	done

clean:
	rm -f MIA*.zip
	rm -rf $(TMP)
