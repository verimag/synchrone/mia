## Development notice

This a project that is still in development and continuous improvement. In
particular, the new Coolidge architecture needs to be properly tested against
real hardware, so use it with caution.

Here follows a non exhaustive list of work needed to be done :

- [ ] Add better deadline management (for each node vs global) to return
  the schedulability
- [ ] Add a config file to indicate the yaml and dat files locations
- [x] Connect the scripts to the original MIA GUI (and stop using matplotlib)
- [x] Check if the bostan bus works well and gives the same schedules as v1
- [ ] Port the other buses from MIA v1
- [x] Add the Coolidge bus
- [ ] Properly test the Coolidge bus
- [ ] add documentation about YAML and .dat files
- [ ] remove pycache from packaging script
- [ ] add more examples
- [ ] add non regression tests
