# MIA - Many-Core Interference Analysis

## Improvements in this version
- Easier maintenance with no inherited code from original C++ version (hereby
  referred to as v1)
- Better performance with a new scheduling algorithm ($\mathcal{O}(n^2)$ vs $\mathcal{O}(n^4)$)

## Extended description

The goal of `rtAnalyser` is to compute the minimal release dates and the
associated scheduling of a task set on a many-core processor, with interference delay estimation and integration.

## Requirements
At least python3.6 is required  

Dependencies: 

- `pyyaml`: Can be installed with `pip3 install --user pyyaml`

## Usage

### Example
```sh
./rtAnalyser.py -profiles examples/profiles -arch Bostan -ts examples/example2_dflow.yaml -o /tmp/output_mia_15245.yaml
```

Run `./rtAnalyser.py -h` for help.

### YAML files

Global fields:

- `numOfCores` = total number of cores
- `deadline` = global deadline
- `nodes` = list of nodes and their specification
 
Node fields:

- `assignedTo` = core identifier
- `traceName` = a trace file of the task containing
    - `procDemand` = WCET of the task
    - `memDemand` = Worst-case number of memory accesses
    - OPTIONAL field: used only when both `procDemand` and 
    `memDemand` fields are not specified 
- `procDemand` = WCET of the task
    - OPTIONAL field: if not specified, will fallback to the one defined in 
    the trace file identified by `traceName` field
- `memDemand` = Worst-case number of memory accesses
    - OPTIONAL field: if not specified, will fallback to the one defined in 
    the trace file identified by `traceName` field
- `release` = an initial release date (offset)
- `deadline` = the deadline of the task. Ignored if =-1 or global deadline >0
- `order` = priority in case of any choice between ready tasks
    - OPTIONAL field: However, it should be present either
      in all nodes or not at all. An assert checks this constraint
- `deps` = list of dependencies, predecessors in the DAG
- `accessTo` = list of the successors' keys and number of 
           accesses to communicate to this successor
- `writeAccesses` = worst-case number of accesses along the wriite phasis
    - OPTIONAL field: to be used by the -cw option
               
Specific fields for a given architecture:

- `nbTransactions` = spec. Bostan: nb of bursts of accesses
             `memDemand` accesses are grouped in those Transactions
- `memDemand_IC` = Spec Coolidge_0 to 3: worst-case number of instruction cache accesses,
- `memDemand_DC` = Spec Coolidge_0 to 3: worst-case number of data cache accesses, 
- `memDemand_UNC` = Spec Coolidge_0 to 3: worst-case number of uncached accesses, 
          
Specific fields for the NOC:

- `RXAccesses` = specification of the RX accesses on the model of a node specification

See more details in `example/multi_cluster.yaml`

## Extending MIA

### How to add an architecture to MIA v2?

Adding an architecture demands to write two functions : `NAME_bus_arbiter` and
`NAME_max_interference` in the file `src/architectures.py`.

- `NAME_bus_arbiter(dnodes, node_set, k, s)` computes the isolated interference
  of all nodes in `node_set`, interfering on bank `k`, and adds it to
  `nodes.virtual_interference[k]` for each node if `s = 1`, subtracts it
  otherwise.

- `NAME_max_interference(node, k)` is the maximum amount of interference that a
  node can get from its r/w accesses on bank `k`.
