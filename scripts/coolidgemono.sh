#!/usr/bin/env bash
#
# WCET code generation, run on real hardware, then extract information for mia,
# run mia analysis and then finally run prem1 code generation with proper
# rel_dates

# Constants
readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
readonly ROOT_FOLDER="$SCRIPT_DIR/.."
readonly EXAMPLES_FOLDER="$ROOT_FOLDER/examples"

# Arguments
out_arg="$1"

# Check if out_arg was supplied
if [[ -z $out_arg ]]; then
  out_folder="$(mktemp -t -d coolidgemono__sh.XXXXXX)"
else
  out_folder="$out_arg"
fi

mkdir -p "$out_folder"

for example in "$EXAMPLES_FOLDER"/*.yaml
do
	filename=$(basename -- "$example")
	extension="${filename##*.}"
	filename="${filename%.*}"
	echo "${filename}" | tee -a "$out_folder/coolidgemono.log"
	$ROOT_FOLDER/src/rtAnalyser.py -profiles $EXAMPLES_FOLDER/profiles \
		-arch CoolidgeMono -ts $example \
		-o $out_folder/$filename-out.yaml \
		> >(tee -a "$out_folder/coolidgemono.log") \
		2> >(tee -a "$out_folder/coolidgemono.log" >&2)
		# -sap 4 \
done

