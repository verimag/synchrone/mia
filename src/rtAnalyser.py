#!/usr/bin/env python3

# MIA (Many-Core Interferece Analysis) - Tool that computes minimal release dates
# of a given scheduled task set on a many-core processor, taking interference into
# account
# Copyright (C) 2019-2020 Verimag
#
# This file is part of MIA (Many-Core Interferece Analysis).
#
# MIA (Many-Core Interferece Analysis) is free software: you can redistribute it
# and/or modify
# it under the terms of the CeCILL-C License as published by the CEA - CNRS -
# INRIA association, either version 2.1 of the License, or (at your option) any
# later version.
#
# MIA (Many-Core Interferece Analysis) is distributed "as is" in the hope that it
# will be
# useful, but WITHOUT ANY OTHER EXPRESS OR TACIT WARRANTY, and in particular,
# without any warranty as to its COMMERCIAL value, its secured, safe, innovative
# or relevant nature.  See the CeCILL-C License for more details.
#
# You should have received a copy of the CeCILL-C License along with
# MIA (Many-Core Interferece Analysis).  If not, see
# <https://cecill.info/licences.en.html>.


"""
    Main script to used to run program

    Parses the arguments, import the `yaml` file as a `dict`, with name `data`,
    runs all functions form `prework.py`, runs the analysis function
    `response_time_analyser`, then runs all functions from `write.py`.
"""

import sys
import argparse

import yaml

import prework
import output
import architectures
import globals

def response_time_analyzer(all_nodes):
    """
        Main function.
    """
    # Compute cache interference for all nodes
    for node in all_nodes:
        if globals.SINGLE_BANK_MODE:
            target_bank = node.cluster * globals.CORES_PER_CLUSTER
        else:
            target_bank = node.core.num
        node.cache_interference = compute_cache_interference(node)
        node.interferences[target_bank] = node.cache_interference
        if node.cache_interference > 0:
            node.interferences_desc[target_bank] = f"{node.cache_interference} = {node.cache_interference}"

    # schedule list to access the next node on a core in O(1) time
    cores_map = [set() for i in range(globals.NUM_OF_CORES + 1)]
    for node in all_nodes:
        if node.core.is_PE():
            cores_map[node.core.num].add(node)
        else:
            cores_map[-1].add(node)
    schedule = [sorted(list(core), key=lambda node: -1 * node.order) for core in cores_map[:-1]]
    schedule.append(cores_map[-1])  # RX, as a set
    if globals.VERBOSE:
        print(schedule)
    # (schedule[c] is the stack (reversed list) of scheduled nodes on core c)

    # fixed release times
    minimal_releases_dates = set()
    for node in all_nodes:
        if node.rel > 0:
            minimal_releases_dates.add(node.rel)

    alive_nodes = set()
    t = 0

    while True:
        if globals.VERBOSE:
            print(f"\nt={t}:")

        # Nodes closing at that time
        closed_nodes = set()
        for node in alive_nodes:
            if node.ends_at(t):
                closed_nodes.add(node)
                for rev_dep in node.rev_deps:
                    rev_dep.deps.remove(node)
        alive_nodes -= closed_nodes

        # Nodes opening at that time
        opened_nodes = set()
        # Tasks nodes
        for k in range(globals.NUM_OF_CORES):
            if schedule[k] != []:
                next_node = schedule[k][-1]
                if next_node.is_ready() and (next_node.rel == 0 or next_node.rel <= t):
                    schedule[k].pop()
                    opened_nodes.add(next_node)
                    if next_node.rel > 0:
                        minimal_releases_dates.discard(next_node.rel)
                    next_node.rel = t
        # RX nodes
        RX = -1
        opened_RX_nodes = set()
        for next_node in schedule[RX]:
            if next_node.is_ready() and (next_node.rel == 0 or next_node.rel <= t):
                opened_RX_nodes.add(next_node)
                opened_nodes.add(next_node)
                if next_node.rel > 0:
                    minimal_releases_dates.discard(next_node.rel)
                next_node.rel = t
        schedule[RX] -= opened_RX_nodes
        alive_nodes.update(opened_nodes)

        if globals.VERBOSE:
            print(f"  NODES -- alive: {alive_nodes},  opened: {opened_nodes},  closed: {closed_nodes}")
            print("  SCHED -- " + ",  ".join([f"PE{p}: {list}" for p, list in enumerate(schedule)]))

        for dest_node in alive_nodes:
            for source_node in alive_nodes:
                if source_node != dest_node:
                    if globals.SINGLE_BANK_MODE:
                        list_of_banks = [i*globals.NUM_OF_CLUSTERS for i in range(globals.NUM_OF_BANKS)]
                    else:
                        list_of_banks = range(globals.NUM_OF_BANKS)
                    for k in list_of_banks:
                        if source_node.accesses(k) and dest_node.accesses(k):
                            if source_node not in dest_node.interferences_sets[k]:
                                dest_node.interferences_sets[k].add(source_node)
                                node_set = dest_node.interferences_sets[k]
                                dest_node.interferences[k] = compute_interference(dest_node, node_set, k)

        # time of the nearest closing node
        t_next = float('inf')
        for node in alive_nodes:
            if t < node.end() < t_next:
                t_next = min(t_next, node.end())
        for release in minimal_releases_dates:
            if t < release < t_next:
                t_next = min(t_next, release)
        if t_next == float('inf'):
            return t
        t = t_next


def compute_interference(node, node_set, k):
    return getattr(architectures, globals.ARCH).bus_arbiter(node, node_set, k)


def compute_cache_interference(node):
    return getattr(architectures, globals.ARCH).cache_arbiter(node)


if __name__ == "__main__":
    # Argument parsing
    parser = argparse.ArgumentParser(
        description="Reads a task graph input file and computes release " \
        "dates and response times",
        epilog="return values: Schedulable | Not schedulable")
    parser.add_argument('-arch', type=str,
                        help='architecture config file  \
                             (No inter, RoundRobin, Bostan, CoolidgeMono,  \
                             Coolidge_i where i is 0, 1, 2 or 3)')
    parser.add_argument('-ts', type=str,
                        help="input .yaml file (select benchmark in old doc)")
    parser.add_argument('-profiles', type=str,
                        default='../examples/profiles/baseArchitecture',
                        help="give the path where profiles are generated")
    parser.add_argument('-o', '--output', type=str, default='out.yaml',
                        help='output .yaml file')
    parser.add_argument('-sb', action="store_true",
                        help="use single bank memory mode (default: multi)")
    parser.add_argument('-cw', action='store_true',
                        help="use dual phase model (default: single)")
    parser.add_argument("-sap", type=int, default=2,
                        help='SAP parameter value (only for Coolidge* \
                                architectures)')
    parser.add_argument('-v', '--verbose', default=False, action='store_true')
    parser.add_argument('-e', '--explain', default=False, action='store_true')

    # If script is called without arguments
    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    args = parser.parse_args()

    filename = args.ts
    profiles_path = args.profiles + '/baseArchitecture/'
    out_file = args.output
    globals.ARCH = args.arch
    globals.DUAL_PHASE_MODEL = args.cw
    globals.SINGLE_BANK_MODE = args.sb
    globals.VERBOSE = args.verbose
    globals.EXPLAIN = args.explain
    globals.COOLIDGE = (args.arch[0] == 'C')

    if args.sap == 0:
        raise Exception("SAP parameter can't be zero")
    else:
        globals.SAP_PARAM = args.sap

    if globals.VERBOSE:
        print("--- verbose active ---",
              "Running with following args:",
              f"arch: {globals.ARCH}",
              f"ts: {filename}",
              f"profiles: {profiles_path}",
              f"output: {out_file}",
              f"dual phase model: {globals.DUAL_PHASE_MODEL}",
              f"single bank: {globals.SINGLE_BANK_MODE}",
              "", sep='\n')

    # Open YAML file
    with open(filename, 'r') as stream:
        try:
            data = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)

    # Set calculated globals
    globals.CORES_PER_CLUSTER = data.get('coresPerCluster', 16)
    globals.NUM_OF_CLUSTERS = data.get('numOfClusters', 5)
    # TODO: check what numOfCores on the YAML is supposed to be exactly and why
    # it's not used here
    globals.NUM_OF_CORES = globals.CORES_PER_CLUSTER * globals.NUM_OF_CLUSTERS
    globals.NUM_OF_BANKS = globals.NUM_OF_CLUSTERS if globals.SINGLE_BANK_MODE \
                            else globals.NUM_OF_CORES
    globals.DEADLINE = data.get('deadline', 100000)  # Default deadline value.

    # Prework
    if not (prework.check_YAML(data)):
        globals.Logger.log("Found no procDemand and/or memDemand in YAML, backup to .dat")
        prework.import_values_from_dat(data, profiles_path)
    else:
        dat_files = prework.get_traces_if_exist(data, profiles_path)
        if (len(dat_files) != 0):
            print("WARNING: Found procDemand & memDemand in YAML file, but "\
                  "trace files {} exist. Will ignore traces".format(dat_files))
    all_nodes = prework.get_all_nodes(data)
    prework.verify_dependencies(all_nodes)
    prework.add_deps_from_order(all_nodes)
    prework.verify_graph_and_add_order_from_deps(all_nodes)
    if globals.DUAL_PHASE_MODEL:
        prework.dual_phase(all_nodes)
    prework.compute_reverse_dependencies(all_nodes)
    prework.compute_banks_mem_demands(all_nodes)

    # RTA
    duration = response_time_analyzer(all_nodes)

    # Results
    deadline = data.get('deadline', None)
    schedulable = False
    if deadline is not None and duration > deadline:
        print(f"\n    Not schedulable, duration {duration} exceeds deadline {deadline}.\n")
    else:
        print(f"\n    Schedulable with duration {duration}.\n")
        schedulable = True

    data['schedulable'] = int(schedulable)
    data['duration'] = duration

    # Output
    output.all_nodes_to_data(all_nodes, data)
    output.cleanup(data)
    output.output_yaml(data, out_file)
