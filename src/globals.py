
# MIA (Many-Core Interferece Analysis) - Tool that computes minimal release dates
# of a given scheduled task set on a many-core processor, taking interference into
# account
# Copyright (C) 2019-2020 Verimag
#
# This file is part of MIA (Many-Core Interferece Analysis).
#
# MIA (Many-Core Interferece Analysis) is free software: you can redistribute it
# and/or modify
# it under the terms of the CeCILL-C License as published by the CEA - CNRS -
# INRIA association, either version 2.1 of the License, or (at your option) any
# later version.
#
# MIA (Many-Core Interferece Analysis) is distributed "as is" in the hope that it
# will be
# useful, but WITHOUT ANY OTHER EXPRESS OR TACIT WARRANTY, and in particular,
# without any warranty as to its COMMERCIAL value, its secured, safe, innovative
# or relevant nature.  See the CeCILL-C License for more details.
#
# You should have received a copy of the CeCILL-C License along with
# MIA (Many-Core Interferece Analysis).  If not, see
# <https://cecill.info/licences.en.html>.

"""
    Global variables parsed.
"""

# Variables
DEADLINE = None
SINGLE_BANK_MODE = None
DUAL_PHASE_MODEL = None
VERBOSE = None
EXPLAIN = None
ARCH = None
NUM_OF_CORES = None
NUM_OF_BANKS = None
NUM_OF_CLUSTERS = None
CORES_PER_CLUSTER = None
SAP_PARAM = None

# TODO: Remove this when WCET for Coolidge can be properly calculated
COOLIDGE = None

class Logger:
    def log(msg):
        if VERBOSE:
            print(msg)
