
# MIA (Many-Core Interferece Analysis) - Tool that computes minimal release dates
# of a given scheduled task set on a many-core processor, taking interference into
# account
# Copyright (C) 2019-2020 Verimag
#
# This file is part of MIA (Many-Core Interferece Analysis).
#
# MIA (Many-Core Interferece Analysis) is free software: you can redistribute it
# and/or modify
# it under the terms of the CeCILL-C License as published by the CEA - CNRS -
# INRIA association, either version 2.1 of the License, or (at your option) any
# later version.
#
# MIA (Many-Core Interferece Analysis) is distributed "as is" in the hope that it
# will be
# useful, but WITHOUT ANY OTHER EXPRESS OR TACIT WARRANTY, and in particular,
# without any warranty as to its COMMERCIAL value, its secured, safe, innovative
# or relevant nature.  See the CeCILL-C License for more details.
#
# You should have received a copy of the CeCILL-C License along with
# MIA (Many-Core Interferece Analysis).  If not, see
# <https://cecill.info/licences.en.html>.

import yaml

import globals


def all_nodes_to_data(all_nodes, data):
    """
        Adds the computed values back into the data dict.
    """
    if globals.DUAL_PHASE_MODEL:
        # Add "write-X" nodes in the yaml
        for node in all_nodes:
            if node.type == "W":
                data['nodes'][node.name] = {
                     'traceName': node.trace_name,
                     'assignedTo': node.core.num}

    for node in all_nodes:
        if node.core.is_PE():
            dnode = data['nodes'][node.name]
            dnode['assignedTo'] = node.core.num
        if node.core.is_RX():
            dnode = data['RxAccesses'][node.name]
        dnode['release'] = node.rel
        dnode['response'] = node.R()
        dnode['interference'] = node.interference()
        dnode['deadline'] = node.deadline
        if globals.EXPLAIN:
            dnode['interference_description'] = \
                [[bank, dsc] for bank, dsc in node.interferences_desc.items()]

    # In case numOfCores is wrong (this happens quite often):
    max_core = 0
    for node in all_nodes:
        max_core = max(max_core, node.core.num)
    data['numOfCores'] = max_core + 1  # (cores start at 0)


# TODO: Implement
def cleanup(data):
    """
        Removes useless fields to output a cleaner yaml file.
    """

# TODO: implement
def schedulable():
    """
        Computes the schedulability of the computed schedule.
    """

def output_yaml(data, output_filepath):
    """
        Writes the yaml file with the calculated release dates.
    """
    with open(output_filepath, 'w') as outfile:
        yaml.dump(data, outfile, default_flow_style=None)
