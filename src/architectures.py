
# MIA (Many-Core Interferece Analysis) - Tool that computes minimal release dates
# of a given scheduled task set on a many-core processor, taking interference into
# account
# Copyright (C) 2019-2020 Verimag
#
# This file is part of MIA (Many-Core Interferece Analysis).
#
# MIA (Many-Core Interferece Analysis) is free software: you can redistribute it
# and/or modify
# it under the terms of the CeCILL-C License as published by the CEA - CNRS -
# INRIA association, either version 2.1 of the License, or (at your option) any
# later version.
#
# MIA (Many-Core Interferece Analysis) is distributed "as is" in the hope that it
# will be
# useful, but WITHOUT ANY OTHER EXPRESS OR TACIT WARRANTY, and in particular,
# without any warranty as to its COMMERCIAL value, its secured, safe, innovative
# or relevant nature.  See the CeCILL-C License for more details.
#
# You should have received a copy of the CeCILL-C License along with
# MIA (Many-Core Interferece Analysis).  If not, see
# <https://cecill.info/licences.en.html>.

"""
Architectures and bus arbiters definitions.

Reminder:

 - node.w(k) (write) is the sum of accessTo from a node to others on bank k,
 - node.r(k) (read) is the total amount of data read by the node (memDemand)
    on bank k,
 - node.a(k) (accesses) is the number of unique reading transactions
    (nbTransactions).
"""

import globals


class NoInter:
    """
        No interferences.
    """
    @staticmethod
    def bus_arbiter(node, node_set, k):
        inter = 0
        return inter

    @staticmethod
    def cache_arbiter(node):
        return 0


class RoundRobin:
    """
        Classic Round-Robin arbiter
    """
    @staticmethod
    def bus_arbiter(node, node_set, k):
        inter = 0
        inter_desc = ""

        if node.core.is_PE():
            sources = [0] * globals.NUM_OF_CORES
            for src_node in node_set:
                if src_node.core.is_PE():
                    sources[src_node.core.num] += src_node.w(k) + src_node.r(k)
            # Round-robin for cores
            for c in range(globals.NUM_OF_CORES):
                inter += min(node.w(k) + node.a(k), sources[c])
                if sources[c] > 0:
                    inter_desc += f" + min(" \
                        f"{node.w(k) + node.a(k)}, {sources[c]})"
            if inter != 0:
                node.interferences_desc[k] = inter_desc
        return inter

    @staticmethod
    def cache_arbiter(node):
        return 0


class Bostan:
    """
        MPPA2 (MPPA256) Bostan.
    """
    @staticmethod
    def bus_arbiter(node, node_set, k):
        BURST_SIZE = 8

        inter = 0
        inter_desc = ""
        if node.core.is_PE():
            RX = -1
            sources = [0] * (globals.NUM_OF_CORES + 1)
            for src_node in node_set:
                if src_node.core.is_PE():
                    sources[src_node.core.num] += src_node.w(k) + src_node.r(k)
                else:
                    sources[RX] += src_node.w(k) + src_node.r(k)
            # Round-robin for cores
            for c in range(globals.NUM_OF_CORES):
                inter += min(BURST_SIZE * (node.w(k) + node.a(k)), sources[c])
                if sources[c] > 0:
                    inter_desc += f" + min({BURST_SIZE} * " \
                        f"{node.w(k) + node.a(k)}, {sources[c]})"
            # Fixed-priority for RX
            inter += sources[RX]
            inter_desc = f"{inter} = {sources[RX]}" + inter_desc
            if inter != 0:
                node.interferences_desc[k] = inter_desc
        return inter

    @staticmethod
    def cache_arbiter(node):
        return 0


class CoolidgeMono:
    """
        MPPA3 (MPPA80) Coolidge Monocluster
    """
    @staticmethod
    def sap_interference(demand1, demand2):
        # demand: number of accesses by each master subjected to arbitration
        return globals.SAP_PARAM * min(demand1, demand2)

    @staticmethod
    def bus_arbiter(node, node_set, k):
        inter = 0
        inter_desc = ""
        if node.core.is_PE():
            sources = [0] * (globals.NUM_OF_CORES + 1)
            for src_node in node_set:
                if src_node.core.is_PE():
                    sources[src_node.core.num] += src_node.w(k) + src_node.r(k)
            # SAP arbitration
            for c in range(globals.NUM_OF_CORES):
                inter += CoolidgeMono.sap_interference(node.w(k) + node.a(k),
                                                       sources[c])
                if sources[c] > 0:
                    inter_desc += f" + {globals.SAP_PARAM} * " \
                        f"min({node.w(k) + node.a(k)}, {sources[c]})"
            if inter != 0:
                node.interferences_desc[k] = inter_desc
        return inter

    @staticmethod
    def cache_arbiter(node):
        return 0


class Coolidge:
    """
        MPPA3 (MPPA80) Coolidge
    """
    @staticmethod
    def sap_interference(demand1, demand2):
        # demand is the number of accesses issued by each master subjected to
        # arbitration
        return globals.SAP_PARAM * min(demand1, demand2)

    @staticmethod
    def drr_interference(demand1, demand2):
        # demand is the number of accesses issued by each master subjected to
        # arbitration
        # https://en.wikipedia.org/wiki/Deficit_round_robin
        # The actual implementation on Coolidge arbitrates on the address,
        # consuming always the same quantum. In practice this deficit round
        # robin then becomes a regular RR
        return min(demand1, demand2)

    @staticmethod
    def intra_cluster_arbitration(node, node_set, k):
        total_inter = 0
        inter_desc = ""
        axi_write = 0
        axi_read = 0
        for node2 in node_set:
            if node2.proc_in_cluster(k):
                total_inter += Coolidge.sap_interference(
                    node.w(k) + node.a(k),
                    node2.w(k) + node2.r(k))
                inter_desc += f" + {globals.SAP_PARAM} * " \
                    f"min({node.w(k) + node.a(k)}, " \
                    f"{node2.w(k) + node2.r(k)})"
            else:
                axi_write += node2.w(k)
                axi_read += node2.r(k)

        if axi_write != 0:
            total_inter += \
                Coolidge.sap_interference(node.w(k) + node.a(k), axi_write)
            inter_desc += f" + {globals.SAP_PARAM} * " \
                f"min({node.w(k) + node.a(k)}, {axi_write})"

        if axi_read != 0:
            total_inter += \
                Coolidge.sap_interference(node.w(k) + node.a(k), axi_read)
            inter_desc += f" + {globals.SAP_PARAM} * " \
                f"min({node.w(k) + node.a(k)}, {axi_read})"

        if (node.core.num == k) or (globals.SINGLE_BANK_MODE and \
                node.cluster * globals.CORES_PER_CLUSTER == k):
            total_inter += node.cache_interference
            inter_desc = f"{total_inter} = {node.cache_interference}" + inter_desc
        else:
            inter_desc = f"{total_inter} =" + inter_desc[2:]

        if total_inter != 0:
            node.interferences_desc[k] = inter_desc

        return total_inter

    @staticmethod
    def inter_cluster_arbitration(node, node_set, k):
        total_inter = 0
        inter_desc = ""
        # First arbiter: SAP
        interference_per_cluster = [0] * globals.NUM_OF_CLUSTERS
        for node2 in node_set:
            # If both nodes are on the same cluster and target a distant one
            # TODO: as there are different R/W AXI virtual banks, should this
            # be distinguished to reduce pessimism?
            if node2.cluster == node.cluster:
                interference_per_cluster[node2.cluster] += \
                    Coolidge.sap_interference(node.w(k) + node.a(k),
                                                node2.w(k) + node2.r(k))
                inter_desc += f" + {globals.SAP_PARAM} * " \
                    f"min({node.w(k) + node.a(k)}, " \
                    f"{node2.w(k) + node2.r(k)})"
            # Otherwise the interference is just propagated to the next arbiter
            else:
                interference_per_cluster[node2.cluster] += node2.w(k) + node2.r(k)
        interference_SAP1 = interference_per_cluster[node.cluster]
        total_inter += interference_SAP1
        # Second arbiter: DRR
        interference_DRR = 0
        for i in range(globals.NUM_OF_CLUSTERS):
            if i != node.cluster and interference_per_cluster[i] > 0:
                interference_DRR += Coolidge.drr_interference(
                    node.w(k) + node.a(k) + total_inter,
                    interference_per_cluster[i])
                inter_desc += f" + " \
                    f"min({node.w(k) + node.a(k) + interference_SAP1}, " \
                    f"{interference_per_cluster[i]})"
        total_inter += interference_DRR
        # Third and last arbiter: another SAP
        interference_SAP2 = 0
        for node2 in node_set:
            if node2.proc_in_cluster(k):
                interference_SAP2 += Coolidge.sap_interference(
                    node.w(k) + node.a(k) + total_inter,
                    node2.w(k) + node2.r(k))
                inter_desc += f" + {globals.SAP_PARAM} * " \
                    f"min({node.w(k) + node.a(k) + interference_SAP1 + interference_DRR}, " \
                    f"{node2.w(k) + node2.r(k)})"
        total_inter += interference_SAP2
        inter_desc = f"{total_inter} =" + inter_desc[2:]
        if total_inter != 0:
            node.interferences_desc[k] = inter_desc
        return total_inter

    @staticmethod
    def get_axi_traversal_time(proc_idx1, proc_idx2):
        cluster_phy_weights = {}
        cluster_phy_weights[(0,1)] = 108
        cluster_phy_weights[(0,2)] = 100
        cluster_phy_weights[(0,3)] = 92
        cluster_phy_weights[(0,4)] = 100
        cluster_phy_weights[(1,2)] = 124
        cluster_phy_weights[(1,3)] = 116
        cluster_phy_weights[(1,4)] = 92
        cluster_phy_weights[(2,3)] = 92
        cluster_phy_weights[(2,4)] = 116
        cluster_phy_weights[(3,4)] = 108

        i2 = min(proc_idx1, proc_idx2)
        j2 = max(proc_idx1, proc_idx2)

        cluster_idx1 = i2 // globals.CORES_PER_CLUSTER
        cluster_idx2 = j2 // globals.CORES_PER_CLUSTER
        return cluster_phy_weights[(cluster_idx1, cluster_idx2)]


    @staticmethod
    def bus_arbiter(node, node_set, k):
        # If you write on your own cluster
        if node.proc_in_cluster(k):
            return Coolidge.intra_cluster_arbitration(node, node_set, k)
        # Or if you are writing on another cluster
        else:
            interf = Coolidge.inter_cluster_arbitration(node, node_set, k)
            comm_traversal_time = (node.w(k) + node.r(k)) * \
                    Coolidge.get_axi_traversal_time(node.core.num, k)
            if globals.VERBOSE:
                print('interf', interf)
                print('comm_traversal_time', comm_traversal_time)
            return interf + comm_traversal_time


class Coolidge_0(Coolidge):

    @staticmethod
    def cache_arbiter(node):
        "Impact of cache analysis taken into account with the WCET"
        return 0;

class Coolidge_1(Coolidge):

    @staticmethod
    def cache_arbiter(node):
        # The first term of the sum is due to cached access, the second to
        # uncached access
        return min(node.mem_demand_IC, node.mem_demand_DC) + node.mem_demand_IC * 20
        # (20 is the capacity of the prefetch buffer added to the LSU pipeline)


class Coolidge_2(Coolidge):

    @staticmethod
    def cache_arbiter(node):
        "Max LD.U at each IC access"
        inter = 0
        if node.mem_demand_UNC != []:
            inter = node.mem_demand_IC * max(node.mem_demand_UNC)[0]
        # The first term of the sum is due to cached access, the second to
        # uncached access
        return min(node.mem_demand_IC, node.mem_demand_DC) + inter


class Coolidge_3(Coolidge):

    @staticmethod
    def cache_arbiter(node):
        "Multiset of Max LD.U normalized to frequence that they may happen"
        inter = 0
        countdown = node.mem_demand_IC
        multiset = sorted(node.mem_demand_UNC)
        while countdown > 0 and multiset != []:
            uncached_seq = multiset.pop()
            if countdown <= uncached_seq[1]:
                inter += countdown * uncached_seq[0]
                countdown = 0
            else:
                inter += uncached_seq[1] * uncached_seq[0]
                countdown -= uncached_seq[1]
        # The first term of the sum is due to cached access, the second to
        # uncached access
        return min(node.mem_demand_IC, node.mem_demand_DC) + inter
