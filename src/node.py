
# MIA (Many-Core Interferece Analysis) - Tool that computes minimal release dates
# of a given scheduled task set on a many-core processor, taking interference into
# account
# Copyright (C) 2019-2020 Verimag
#
# This file is part of MIA (Many-Core Interferece Analysis).
#
# MIA (Many-Core Interferece Analysis) is free software: you can redistribute it
# and/or modify
# it under the terms of the CeCILL-C License as published by the CEA - CNRS -
# INRIA association, either version 2.1 of the License, or (at your option) any
# later version.
#
# MIA (Many-Core Interferece Analysis) is distributed "as is" in the hope that it
# will be
# useful, but WITHOUT ANY OTHER EXPRESS OR TACIT WARRANTY, and in particular,
# without any warranty as to its COMMERCIAL value, its secured, safe, innovative
# or relevant nature.  See the CeCILL-C License for more details.
#
# You should have received a copy of the CeCILL-C License along with
# MIA (Many-Core Interferece Analysis).  If not, see
# <https://cecill.info/licences.en.html>.

"""
    Node and Core classes definitions.
"""

import globals


class Core:
    """
        Description of the core a node belongs to.
    """
    def __init__(self, num):
        self.num = num

    def is_PE(self):
        return 0 <= self.num < globals.NUM_OF_CORES

    def is_RX(self):
        return self.num == - 10


class Node:
    """
        Descriptions of nodes.
    """
    def __init__(self, data, name, location):
        data_node = data[name]

        self.name = name
        self.type = None  # Is either "W" or "C" in dual phase mode
        try:
            self.trace_name = data_node['traceName']
        except KeyError:
            self.trace_name = None
        if location == "PE":
            self.core = Core(data_node['assignedTo'])
        elif location == "RX":
            self.core = Core(-10)  # MIAViewer needs this for RX
        else:
            raise Exception(f"Unknown location for node {name}.")
        self.cluster = self.core.num // globals.CORES_PER_CLUSTER
        self.order = data_node.get('order', None)
        self.rel = data_node['release']
        self.proc_demand = data_node['procDemand']
        self.deadline = data_node.get('deadline', globals.DEADLINE)

        self.write_accesses = data_node.get('writeAccesses', 0)
        # General memory demand (normally IC+DC misses)
        self.mem_demand = data_node.get('memDemand', 0)
        # Instruction Cache demands
        self.mem_demand_IC = data_node.get('memDemand_IC', 0)
        # Data Cache demands
        self.mem_demand_DC = data_node.get('memDemand_DC', 0)
        # Uncached Data demands, as a multiset with the form
        # [[seq_access_length, number_of_seq_access] for _ in range(len(DC))]
        self.mem_demand_UNC = data_node.get('memDemand_UNC', [])
        # Total uncached Data demands
        total_uncached = 0
        for uncached_seq in self.mem_demand_UNC:
            total_uncached += uncached_seq[0] * uncached_seq[1]
        # Overrides total memory demands only if they are split
        if self.mem_demand_IC != 0 or self.mem_demand_DC != 0 or total_uncached != 0:
            self.mem_demand = self.mem_demand_IC + self.mem_demand_DC + total_uncached
        if globals.VERBOSE:
            print(f'mem_demand: {self.mem_demand}')
        # proc_demand is equivalent to the WCET
        self.proc_demand = data_node['procDemand']
        # The amount of delay added to the instruction cache access request
        self.cache_interference = 0
        # If number of transactions unspecified, worst case applies
        self.nb_transactions = data_node.get('nbTransactions', self.mem_demand)

        list_accesses = data_node.get('accessTo', [])
        self.nodes_mem_demands = {node: amount for [node, amount] in list_accesses}
        self.banks_mem_demands = {}

        self.deps = set(data_node.get('deps', []))
        self.rev_deps = set()

        self.interferences = [0 for _ in range(globals.NUM_OF_CORES)]
        self.interferences_sets = [set() for _ in range(globals.NUM_OF_CORES)]
        self.interferences_desc = {}

    def __repr__(self):
        return self.name

    def __str__(self):
        return f"  Node({self.name}):  " + \
               ",  ".join([f"{key}: {value}" for key, value in self.__dict__.items()])

    def add_to_banks_mem_demands(self, bank, amount):
        if bank not in self.banks_mem_demands:
            self.banks_mem_demands[bank] = 0
        self.banks_mem_demands[bank] += amount

    def writes_on(self, k):
        return self.banks_mem_demands.get(k, 0) > 0

    def reads_on(self, k):
        if globals.SINGLE_BANK_MODE:
            return self.cluster * globals.CORES_PER_CLUSTER == k and self.mem_demand > 0
        else:
            return self.core.num == k and self.mem_demand > 0

    def accesses(self, k):
        return self.reads_on(k) or self.writes_on(k)

    def R(self):
        return self.proc_demand + self.interference()

    def end(self):
        return self.rel + self.proc_demand + self.interference()

    def ends_at(self, t):
        return t == self.rel + self.proc_demand + self.interference()

    def is_ready(self):
        return len(self.deps) == 0

    def w(self, k):
        return self.banks_mem_demands.get(k, 0)

    def r(self, k):
        if globals.SINGLE_BANK_MODE:
            return (self.cluster * globals.CORES_PER_CLUSTER == k) * self.mem_demand
        else:
            return (self.core.num == k) * self.mem_demand

    def a(self, k):
        if globals.SINGLE_BANK_MODE:
            return (self.cluster * globals.CORES_PER_CLUSTER == k) * self.nb_transactions
        else:
            return (self.core.num == k) * self.nb_transactions

    def overlap(self, node):
        return max(min(node.end(), self.end()) - max(node.rel, self.rel), 0)

    def interference(self):
        return sum(self.interferences)

    def proc_in_cluster(self, k):
        return self.cluster == k // globals.CORES_PER_CLUSTER
