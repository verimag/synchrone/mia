
# MIA (Many-Core Interferece Analysis) - Tool that computes minimal release dates
# of a given scheduled task set on a many-core processor, taking interference into
# account
# Copyright (C) 2019-2020 Verimag
#
# This file is part of MIA (Many-Core Interferece Analysis).
#
# MIA (Many-Core Interferece Analysis) is free software: you can redistribute it
# and/or modify
# it under the terms of the CeCILL-C License as published by the CEA - CNRS -
# INRIA association, either version 2.1 of the License, or (at your option) any
# later version.
#
# MIA (Many-Core Interferece Analysis) is distributed "as is" in the hope that it
# will be
# useful, but WITHOUT ANY OTHER EXPRESS OR TACIT WARRANTY, and in particular,
# without any warranty as to its COMMERCIAL value, its secured, safe, innovative
# or relevant nature.  See the CeCILL-C License for more details.
#
# You should have received a copy of the CeCILL-C License along with
# MIA (Many-Core Interferece Analysis).  If not, see
# <https://cecill.info/licences.en.html>.

"""
    Contains all the preparative work to have a nice dictionary of Nodes().
"""
import os.path

from itertools import chain
from copy import deepcopy

import globals
from node import Node

def get_traces_if_exist(data, profiles_path):
    existing_traces = []
    for node, dnode in chain(data['nodes'].items(), data.get('RxAccesses', {}).items()):
        try:
            trace_name = dnode['traceName']
            trace_path = f"{profiles_path}/{trace_name}.dat"
            if os.path.isfile(trace_path):
                existing_traces.append(trace_path)
        except KeyError:
            pass
    return existing_traces

def import_values_from_dat(data, profiles_path):
    """
        Fetches the information contained in the `.dat` files and add it to the
        `data` dict derived from the yaml file

        Works in place
    """
    for node, dnode in chain(data['nodes'].items(), data.get('RxAccesses', {}).items()):
        trace_name = dnode['traceName']
        with open(f"{profiles_path}/{trace_name}.dat", 'r') as dat:
            line = dat.readline()
            while line:
                words = line.split(' ')
                if len(words) == 2:
                    dnode[words[0]] = int(words[1])
                line = dat.readline()
    return data

def dict_from_data(data):
    """
        Converts the data dict into a set of Nodes objects.
    """
    all_nodes_dict = {name: Node(data['nodes'], name, "PE")
                      for name in data['nodes']}
    return all_nodes_dict

def check_YAML(data):
    withoutProcDemand = []
    withoutMemDemand = []
    for n in data['nodes']:
        try:
            data['nodes'][n]['procDemand']
        except KeyError:
            withoutProcDemand.append(n)
        try:
            data['nodes'][n]['memDemand']
        except KeyError:
            withoutMemDemand.append(n)
    if (len(withoutProcDemand) != 0):
        if globals.VERBOSE:
            globals.Logger.log("Node(s) {} had no procDemand".format(withoutProcDemand))
        return False
    if (len(withoutMemDemand) != 0):
        if globals.VERBOSE:
            globals.Logger.log("Node(s) {} had no memDemand".format(withoutMemDemand))
        return False
    return True

def get_all_nodes(data):
    all_nodes_dict = dict_from_data(data)
    if 'RxAccesses' in data:
        all_nodes_dict.update({name: Node(data['RxAccesses'], name, "RX")
                               for name in data['RxAccesses']})
    for node in all_nodes_dict.values():
        node.deps = set([all_nodes_dict[dep] for dep in node.deps])
    all_nodes = set(all_nodes_dict.values())
    # Converts name -> nodes objects in nodes_mem_demands
    for node in all_nodes:
        temp_nodes_mem_demands = {}
        for dest_node_name, amount in node.nodes_mem_demands.items():
            dest_node = all_nodes_dict[dest_node_name]
            temp_nodes_mem_demands[dest_node] = amount
        node.nodes_mem_demands.clear()
        node.nodes_mem_demands.update(temp_nodes_mem_demands)
    return all_nodes


# TODO: Implement
def verify_dependencies(all_nodes):
    """
        Checks if the dependencies and memory accesses are coherent. If not,
        fills the missing ones.
    """


def add_deps_from_order(all_nodes):
    """
        Infers dependencies between neighbor nodes on the same core.
    """
    if any([node.order is not None for node in all_nodes]):
        if not all([node.order is not None for node in all_nodes]):
            raise Exception('Some nodes have order set while others do not.')
        cores_map = [set() for i in range(globals.NUM_OF_CORES)]
        for node in all_nodes:
            if node.core.is_PE():
                cores_map[node.core.num].add(node)
        schedule = [sorted(list(core), key=lambda node: -1 * node.order)
                    for core in cores_map]
        for core in schedule:
            for k in range(1, len(core)):
                core[k-1].deps.add(core[k])


def verify_graph_and_add_order_from_deps(all_nodes):
    """
        Fills the `order` fields of node using the specified dependencies.
        Checks if the sub-graphs of the nodes mapped on the same core are
        acyclic and total.
    """
    cores_map = [set() for i in range(globals.NUM_OF_CORES)]
    for node in all_nodes:
        if node.core.is_PE():
            cores_map[node.core.num].add(node)
    for core in cores_map:
        order = len(core) - 1
        while len(core) > 0:
            flags = {node: True for node in core}
            for node in core:
                for dep in node.deps:
                    if dep in core:
                        flags[dep] = False
            first = [node for node, flag in flags.items() if flag]
            if len(first) > 1:
                raise Exception("Not determinist graph")
            if len(first) == 0:
                raise Exception("Cyclic graph")
            first[0].order = order
            core.remove(first[0])
            order -= 1


def compute_reverse_dependencies(all_nodes):
    """
        Computes the reverse dependencies, i.e.  the list of nodes that depend
        on the current one. Adds this to the rev_dep field of the nodes dict
    """
    for node in all_nodes:
        for d in node.deps:
            d.rev_deps.add(node)


def compute_banks_mem_demands(all_nodes):
    """
        Compute the mem demands grouped by bank.
    """
    if globals.SINGLE_BANK_MODE:
        for node in all_nodes:
            for dest, mem_demand in node.nodes_mem_demands.items():
                node.add_to_banks_mem_demands(dest.cluster * globals.CORES_PER_CLUSTER, mem_demand)
    else:
        for node in all_nodes:
            for dest, mem_demand in node.nodes_mem_demands.items():
                node.add_to_banks_mem_demands(dest.core.num, mem_demand)


def dual_phase(all_nodes):
    """
        Turns the program into dual mode: all nodes are split into a Compute
        node and a Write node.
    """
    new_nodes = set()
    for node in all_nodes:
        if not node.core.is_PE():
            new_nodes.add(node)
            break

        node_c = deepcopy(node)
        node_c.type = "C"
        node_c.order = node.order * 2
        node_c.nodes_mem_demands = {}
        if node.proc_demand - sum(node.nodes_mem_demands.values()) < 0:
            raise Exception(f"task {node.name} mem demand is incoherent")
        node_c.proc_demand = node.proc_demand - sum(node.nodes_mem_demands.values())
        node_c.mem_demand = 0
        new_nodes.add(node_c)

        node_w = deepcopy(node)
        node_w.type = "W"
        node_w.name = "write-" + node_w.name
        node_w.order = node.order * 2 + 1
        node_w.deps = {node_c}
        node_w.proc_demand = sum(node.nodes_mem_demands.values())
        new_nodes.add(node_w)

    all_nodes.clear()
    all_nodes.update(new_nodes)

    all_nodes_dict = {node.name: node for node in all_nodes}
    for node in all_nodes:
        if node.type == "C":
            node.deps = {all_nodes_dict["write-" + dep.name]
                         for dep in node.deps}
        if node.type == "W":
            node.nodes_mem_demands = {all_nodes_dict[n.name]: a
                                      for n, a in node.nodes_mem_demands.items()}
